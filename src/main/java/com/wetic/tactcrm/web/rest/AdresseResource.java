package com.wetic.tactcrm.web.rest;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.wetic.tactcrm.service.AdresseService;
import com.wetic.tactcrm.service.dto.AdresseDTO;
import com.wetic.tactcrm.web.util.RestUtils;

@RestController
@RequestMapping("/api")
public class AdresseResource {

    private final AdresseService adresseService;




    public AdresseResource(AdresseService adresseService) {
        this.adresseService = adresseService;
    }

    @GetMapping("/adresses")
    public ResponseEntity<List<AdresseDTO>> getAllAdresse(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder){
        Page<AdresseDTO> page = adresseService.findAll(pageable);
        HttpHeaders headers  = RestUtils.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/adresses/{id}")
    public ResponseEntity<AdresseDTO> getAdresse(@PathVariable Long id) {
        Optional<AdresseDTO> typeDepenseDTO = adresseService.findOne(id);
        return RestUtils.wrapOrNotFound(typeDepenseDTO, null);
    }

    @PostMapping("/adresses")
    public ResponseEntity<AdresseDTO> createAdresse(@Valid @RequestBody AdresseDTO deviseDTO) throws URISyntaxException {
        AdresseDTO result  = adresseService.save(deviseDTO);
        return ResponseEntity.created(new URI("/api/adresses" + result.getId()))
                .body(result);
    }

    @PutMapping("/adresses")
    public ResponseEntity<AdresseDTO> updateAdresse(@Valid @RequestBody AdresseDTO deviseDTO) throws URISyntaxException {
        if (deviseDTO.getId()==null){
            return null;
        }
        AdresseDTO result = adresseService.save(deviseDTO);
        return ResponseEntity.created(new URI("/api/adresses" + result.getId()))
                .body(result);
    }

    @DeleteMapping("/adresses/{id}")
    public ResponseEntity<Void> deleteAdresse(@PathVariable Long id) throws URISyntaxException{
        adresseService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
