package com.wetic.tactcrm.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.wetic.tactcrm.repository.DetailsFactureRepository;
import com.wetic.tactcrm.service.DetailsFactureService;
import com.wetic.tactcrm.service.dto.DetailsFactureDTO;
import com.wetic.tactcrm.web.util.RestUtils;

@RestController
@RequestMapping("/api")
public class DetailsFactureResource {

    private final DetailsFactureService detailsFactureService;

    public DetailsFactureResource(DetailsFactureService detailsFactureService) {
        this.detailsFactureService = detailsFactureService;
    }

    @GetMapping("/details-factures")
    public ResponseEntity<List<DetailsFactureDTO>> getAllDetailsFacture(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder){
        Page<DetailsFactureDTO> page = detailsFactureService.findAll(pageable);
        HttpHeaders headers  = RestUtils.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        headers.add("Access-Control-Expose-Headers", "X-Total-Count, Link");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/details-factures")
    public ResponseEntity<DetailsFactureDTO> createDetailsFacture(@Valid @RequestBody DetailsFactureDTO detailsFactureDTO) throws URISyntaxException {
        DetailsFactureDTO result  = detailsFactureService.save(detailsFactureDTO);
        return ResponseEntity.created(new URI("/api/details-factures" + result.getId()))
                .body(result);
    }

    @PutMapping("/details-factures")
    public ResponseEntity<DetailsFactureDTO> updateDetailsFacture(@Valid @RequestBody DetailsFactureDTO detailsFactureDTO) throws URISyntaxException {
        if (detailsFactureDTO.getId()==null){
            return null;
        }
        DetailsFactureDTO result = detailsFactureService.save(detailsFactureDTO);
        return ResponseEntity.created(new URI("/api/details-factures" + result.getId()))
                .body(result);
    }

    @DeleteMapping("/details-factures/{id}")
    public ResponseEntity<Void> deleteDetailsFacture(@PathVariable Long id) throws URISyntaxException{

        detailsFactureService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/details-factures/facture/{id}")
    public ResponseEntity<List<DetailsFactureDTO>> getAllDetailsFacturesByFactureId(@PathVariable Long id) {
        List<DetailsFactureDTO> detailsFactureList = detailsFactureService.findDetailFactureByFactureId(id);
        return ResponseEntity.ok().body(detailsFactureList);
    }


}
